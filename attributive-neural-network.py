"""
attributive-neural-network.py -- neural network that attributes its sources
Copyright (c) 2022 Claude Heiland-Allen
SPDX-License-Identifier: AGPL-3.0-only

ref:
Adam: A Method for Stochastic Optimization
by Diederik P. Kingma, Jimmy Ba
<https://arxiv.org/abs/1412.6980>
"""

import csv
import numpy as np
import os
import sys
from tqdm import tqdm

# activation functions

def relu(z):
  return np.maximum(0, z), np.diag(np.maximum(0, np.sign(z)))

def sigmoid(z):
  s = 1 / (1 + np.exp(-z))
  return 2 * s - 1, np.diag(2 * s * (1 - s))

def softmax(z):
  m = np.max(z)
  s = np.exp(z - m) / np.sum(np.exp(z - m))
  return s, np.diag(s) - np.outer(s, s)

# loss functions

def categorical_cross_entropy(truth, model):
  m = np.maximum(1e-25, model)
  n = np.maximum(1e-25, 1 - model)
  return -np.nan_to_num(np.sum(truth * np.log(m)) + np.sum((1 - truth) * np.log(n))), -np.nan_to_num(truth / m - (1 - truth) / n)

# feed-forward neural network

def forward_propagate_input(state, network, data, attr, rng, dropout, dodropout):
  state[0]['x'] = data
  state[0]['dxdi'] = np.diag(np.ones_like(state[0]['x']))
  state[0]['dxda'] = np.outer(data, attr)
  layers = len(network)
  for layer in range(layers):
    weights = network[layer]['w']
    dWda = network[layer]['dwda']
    if 0 < layer and layer + 1 < layers:
      if dodropout:
        mask = rng.choice([0, 1], p=[dropout, 1 - dropout], size=weights.shape)
        weights = weights * mask
    I = np.append(state[layer]['x'], [1])
    dIda = np.append(state[layer]['dxda'], [np.zeros_like(attr)], axis=0)
    z = weights @ I
    dzdi = weights
    dzda = weights @ dIda + I @ dWda
    if layer + 1 < layers:
      x, dxdz = sigmoid(z)
      if 0 < layer and not dodropout:
        x = x * (1 - dropout)
        dxdz = dxdz * (1 - dropout)
    else:
      x, dxdz = softmax(z)
    state[layer + 1]['x'] = x
    state[layer + 1]['dxdi'] = dxdz @ dzdi
    state[layer + 1]['dxda'] = dxdz @ dzda
  return state

# feed-backward neural network

def backward_propagate_error(state, network, dedx):
  layers = len(state)
  for layer in reversed(range(layers)):
    if layer + 1 < layers:
      # "i" input at layer+1 is "x" output at layer
      state[layer]['dedx'] = (state[layer + 1]['dedx'] @ state[layer + 1]['dxdi'])[:-1]
    else:
      state[layer]['dedx'] = dedx
  return state

# train network

def update_weights(state, network, attr, alpha, beta_1, beta_2, epsilon, t, decay):
  for layer in range(len(network)):
    gradient = np.outer(state[layer + 1]['dedx'], np.append(state[layer]['x'], [1]))
    state[layer]['m'] = beta_1 * state[layer]['m'] + (1 - beta_1) * gradient
    state[layer]['v'] = beta_2 * state[layer]['v'] + (1 - beta_2) * gradient * gradient
    m = state[layer]['m'] / (1 - beta_1 ** t)
    v = state[layer]['v'] / (1 - beta_2 ** t)
    dw = - alpha * m / (np.sqrt(v) + epsilon)
    network[layer]['w'] = decay * network[layer]['w'] + dw
    network[layer]['dwda'] = decay * network[layer]['dwda'] + np.multiply.outer(dw, attr)
  return state, network

# classification

def classify(vector):
  ix = np.argmax(vector)
  onehot = np.zeros_like(vector)
  np.put(onehot, ix, 1)
  return onehot

# online stochastic gradient descent

def stochastic_gradient_descent(rng, network, training_data, training_label, training_attribution, validate_data, validate_label, validate_attribution, training_log):
  training_count = training_data.shape[0]
  validate_count = validate_data.shape[0]
  classes = validate_label.shape[1]
  alpha = 1e-5
  beta_1 = 0.9
  beta_2 = 0.999
  epsilon = 1e-8
  decay = 1 - 1e-8
  dropout = 0.5
  state = [ ]
  for layer in range(len(network) + 1):
    state.append({ })
  for layer in range(len(network)):
    state[layer]['m'] = np.zeros_like(network[layer]['w'])
    state[layer]['v'] = np.zeros_like(network[layer]['w'])
  try:
    t = 0
    for epoch in tqdm(range(1000)):
      total_error = 0
      rms_error = 0
      accuracy = 0
      items = np.arange(training_count)
      rng.shuffle(items)
      for item in items:
        t = t + 1
        state = forward_propagate_input(state, network, training_data[item], np.zeros_like(training_attribution[item]), rng, dropout, True)
        error, dedx = categorical_cross_entropy(training_label[item], state[len(network)]['x'])
        total_error = total_error + error
        rms_error = rms_error + error * error
        accuracy = accuracy + int(np.all(training_label[item] == classify(state[len(network)]['x'])))
        state = backward_propagate_error(state, network, dedx)
        state, network = update_weights(state, network, training_attribution[item], alpha, beta_1, beta_2, epsilon, t, decay)
      vtotal_error = 0
      vrms_error = 0
      vaccuracy = np.zeros((classes, classes))
      items = np.arange(validate_count)
      rng.shuffle(items)
      for item in items:
        state = forward_propagate_input(state, network, validate_data[item], np.zeros_like(validate_attribution[item]), rng, dropout, False)
        error, dedx = categorical_cross_entropy(validate_label[item], state[len(network)]['x'])
        vtotal_error = vtotal_error + error
        vrms_error = vrms_error + error * error
        vaccuracy[np.argmax(validate_label[item]), np.argmax(state[len(network)]['x'])] += 1
      training_log.writerow([epoch, total_error / training_count, np.sqrt(rms_error / training_count), vtotal_error / validate_count, np.sqrt(vrms_error / validate_count), 100.0 * accuracy / training_count, 100.0 * np.sum(np.diag(vaccuracy)) / validate_count] + vaccuracy.flatten().tolist())
      if total_error / training_count > 4:
        break
  except KeyboardInterrupt:
    pass
  return state, network

# load csv with float columns, then label, attribution, comment strings

def load_training_data(filename):
  training_data = []
  training_label = []
  training_attribution = []
  training_comment = []
  with open(filename, mode='r', newline='') as f:
    for row in csv.reader(f):
      if len(row) > 0:
        comment = row.pop()
        attribution = row.pop()
        label = row.pop()
        if not np.isnan(np.sum(np.asarray(row, dtype=float))):
          training_data.append(row)
          training_label.append(label)
          training_attribution.append(attribution)
          training_comment.append(comment)

  label_set = sorted(list(set(training_label)))
  label_map = { }
  ix = 0
  for label in label_set:
    onehot = np.zeros(len(label_set))
    np.put(onehot, ix, 1)
    label_map[label] = onehot
    ix = ix + 1
  for ix in range(len(training_label)):
    training_label[ix] = label_map[training_label[ix]]

  attribution_set = sorted(list(set(training_attribution)))
  attribution_map = { }
  ix = 0
  for attribution in attribution_set:
    onehot = np.zeros(len(attribution_set))
    np.put(onehot, ix, 1)
    attribution_map[attribution] = onehot
    ix = ix + 1
  for ix in range(len(training_attribution)):
    training_attribution[ix] = attribution_map[training_attribution[ix]]

  training_data = np.asarray(training_data, dtype=float)
  training_label = np.asarray(training_label, dtype=float)
  training_attribution = np.asarray(training_attribution, dtype=float)
  training_comment = np.asarray(training_comment, dtype=object)
  label_set = np.asarray(label_set, dtype=object)
  attribution_set = np.asarray(attribution_set, dtype=object)
  return training_data, training_label, training_attribution, training_comment, label_set, attribution_set

# load csv with float columns, then label, attribution, comment strings

def load_data(filename, labels, attributions):
  input_data = []
  input_label = []
  input_attribution = []
  input_comment = []
  with open(filename, mode='r', newline='') as f:
    for row in csv.reader(f):
      if len(row) > 0:
        comment = row.pop()
        attribution = row.pop()
        label = row.pop()
        if not np.isnan(np.sum(np.asarray(row, dtype=float))):
          input_data.append(row)
          input_label.append(label)
          input_attribution.append(attribution)
          input_comment.append(comment)

  label_set = labels.tolist()
  label_map = { }
  for label in sorted(list(set(input_label))):
    label_map[label] = np.zeros(len(label_set))
  ix = 0
  for label in label_set:
    onehot = np.zeros(len(label_set))
    np.put(onehot, ix, 1)
    label_map[label] = onehot
    ix = ix + 1
  for ix in range(len(input_label)):
    input_label[ix] = label_map[input_label[ix]]

  attribution_set = attributions.tolist()
  attribution_map = { }
  for attribution in sorted(list(set(input_attribution))):
    attribution_map[attribution] = np.zeros(len(attribution_set))
  ix = 0
  for attribution in attribution_set:
    onehot = np.zeros(len(attribution_set))
    np.put(onehot, ix, 1)
    attribution_map[attribution] = onehot
    ix = ix + 1
  for ix in range(len(input_attribution)):
    input_attribution[ix] = attribution_map[input_attribution[ix]]

  input_data = np.asarray(input_data, dtype=float)
  input_label = np.asarray(input_label, dtype=float)
  input_attribution = np.asarray(input_attribution, dtype=float)
  input_comment = np.asarray(input_comment, dtype=object)
  return input_data, input_label, input_attribution, input_comment

# normalize data

def condition(data, m=None, s=None):
  m = np.mean(data, axis=0) if m is None else m
  s = np.std(data, axis=0) if s is None else s
  return (data - m) / s, m, s

# initialize random number generator

rng = np.random.default_rng()

# parse command line arguments

if len(sys.argv) < 2:
  sys.exit("missing argument (--train, --classify)")

if sys.argv[1] == "--train":
  if len(sys.argv) < 4:
    sys.exit("missing arguments (training.data, outputdir/)")
  data_filename = sys.argv[2]
  training_dir = sys.argv[3]
  try:
    os.makedirs(training_dir)
  except FileExistsError:
    pass

  # load data set
  training_data, training_label, training_attribution, training_comment, label_set, attribution_set = load_training_data(data_filename)

  # condition data
  training_data, training_mean, training_stddev = condition(training_data)

  # split data set into training/validation parts

  training_count = training_data.shape[0]
  items = np.arange(training_count)
  trng = np.random.default_rng(seed=123456789)
  trng.shuffle(items)
  training_count = int(0.9 * training_count)
  training_items = items[:training_count]
  validate_items = items[training_count:]
  validate_data = training_data[validate_items]
  validate_label = training_label[validate_items]
  validate_attribution = training_attribution[validate_items]
  validate_comment = training_comment[validate_items]
  training_data = training_data[training_items]
  training_label = training_label[training_items]
  training_attribution = training_attribution[training_items]
  training_comment = training_comment[training_items]

  # generate random network

  attributes = training_data.shape[1]
  classes = label_set.size
  attributions = attribution_set.size
  hidden = classes
  network = [ {}, {} ]
  network[0]['w'] = rng.standard_normal((hidden, attributes+1))/np.sqrt(attributes+1)
  network[1]['w'] = rng.standard_normal((classes, hidden+1))/np.sqrt(hidden+1)
  for layer in range(len(network)):
    network[layer]['dwda'] = np.zeros(np.append(network[layer]['w'].shape, [attributions]))

  total_weights = 0
  for layer in range(len(network)):
    total_weights = total_weights + network[layer]['w'].size

  # train network

  with open(training_dir + os.path.sep + "training.log", 'w', newline='') as f:
    training_log = csv.writer(f, delimiter=' ')
    state, network = stochastic_gradient_descent(rng, network, training_data, training_label, training_attribution, validate_data, validate_label, validate_attribution, training_log)

  # save network
  attributions = np.sum(training_attribution, axis=0)
  np.savez(training_dir + os.path.sep + "network.npz", label=label_set, attribution=attribution_set, mean=training_mean, stddev=training_stddev, attributions=attributions, w_0=network[0]['w'], dwda_0=network[0]['dwda'], w_1=network[1]['w'], dwda_1=network[1]['dwda'])

elif sys.argv[1] == "--classify":

  if len(sys.argv) < 4:
    sys.exit("missing arguments (network.npz, input.data)")
  network_filename = sys.argv[2]
  input_filename = sys.argv[3]

  # load network
  npz = np.load(network_filename, allow_pickle=True)

  # load data

  input_data, input_label, input_attribution, input_comment = load_data(input_filename, npz['label'], npz['attribution'])

  # condition data

  input_data, _, _ = condition(input_data, npz['mean'], npz['stddev'])

  # prepare network

  network = [ {}, {} ]
  network[0]['w'] = npz['w_0']
  network[0]['dwda'] = npz['dwda_0']
  network[1]['w'] = npz['w_1']
  network[1]['dwda'] = npz['dwda_1']

  # prepare state

  state = [ ]
  for layer in range(len(network) + 1):
    state.append({ })

  # attribution report
  print("<!DOCTYPE html>\n<html><head><meta charset='UTF-8'><title>attributive neural network</title><style>table { width: 100% }</style></head><body><table>")
  nc = npz['label'].size
  na = npz['attribution'].size
  print(f"<tr><th colspan='2' rowspan='2'>input</th><th colspan='{nc}'>classified genre (bold is genre from track)</th><th rowspan='2'>error</th><th colspan='{na}>attribution for classification (bold is attribution of track)</th></tr><tr>")
  for c in range(nc):
    x = npz['label'][c]
    print(f"<th>{x}</th>")
  for c in range(na):
    x = npz['attribution'][c]
    print(f"<th>{x}</th>")
  print("</tr>")
  for item in range(len(input_data)):
    src = input_comment[item][input_comment[item].find('/') + 1:]
    print(f"<tr><td><audio controls src='https://archive.org/download/{src}'><a href='https://archive.org/download/{src}'>download</a></audio></td><td>{src}</td>")
    state = forward_propagate_input(state, network, input_data[item], np.zeros_like(npz['attribution']), rng, 0.5, False)
    for c in range(input_label[item].size):
      x = state[len(network)]['x'][c]
      if input_label[item][c] > 0.5:
        print(f"<td><b>{x:.2%}</b></td>")
      else:
        print(f"<td>{x:.2%}</td>")
    error, _ = categorical_cross_entropy(input_label[item], state[len(network)]['x'])
    print(f"<td>{error}</td>")
    a = state[len(network)]['x'] @ (state[len(network)]['dxda'] * state[len(network)]['dxda']) / npz['attributions']
    a = np.asarray(a, dtype=float)
    a, _ = softmax(np.sqrt(a))
    a = a * npz['attribution'].size
    a = a - 1
    for c in range(input_attribution[item].size):
      x = a[c]
      if input_attribution[item][c] > 0.5:
        print(f"<td><b>{x:+.2%}</b></td>")
      else:
        print(f"<td>{x:+.2%}</td>")
    print("</tr>")
  print("</table></body></html>")

else:
  sys.exit("unknown argument (expected --train, --classify)")
