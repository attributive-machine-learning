--[[
attributive-markov-chain.lua -- Markov chain that attributes its sources
Copyright (c) 2022,2023 Claude Heiland-Allen
SPDX-License-Identifier: AGPL-3.0-only
--]]

dofile("table.save-1.0.lua")

usage =
"lua attributive-markov-chain.lua build chain.lua order source [source ...]\n" ..
"lua attributive-markov-chain.lua generate chain.lua prompt > out.html\n" ..
"lua attributive-markov-chain.lua censor chain.lua order phrase [phrase ...]\n" ..
"lua attributive-markov-chain.lua prune chain.lua\n"

-- is this really 100% safe?
local function htmlSafe(s)
  if s
  then
    do
      local special =
              { [" "] = "&nbsp;"
              , ["\n"] = "<br />"
              , ["&"] = "&amp;"
              , ["<"] = "&lt;"
              , [">"] = "&gt;"
              , ["'"] = "&apos;"
              , ["\""] = "&quot;"
              }
      return s:gsub(".", special)
    end
  else
    do
      return ""
    end
  end
end

local function readText(path)
  local file = io.open(path, "rb")
  if not file then return nil end
  local content = file:read("*all")
  file:close()
  return { content = content, attribution = htmlSafe(path) }
end

local function compile(order, text)
  local chain = { }
  for start = 1, string.len(text.content) - order - 1, 1
  do
    local context = string.sub(text.content, start, start + order - 1)
    local token = string.sub(text.content, start + order, start + order)
    chain[context] = chain[context] or { }
    chain[context][token] = chain[context][token] or { weight = 0, attribution = {} }
    chain[context][token].attribution[text.attribution] = chain[context][token].attribution[text.attribution] or 0
    chain[context][token].weight = chain[context][token].weight + 1
    chain[context][token].attribution[text.attribution] = chain[context][token].attribution[text.attribution] + 1
  end
  return chain
end

local function mergeInto(chain, a) -- mutates chain
  for context,b in pairs(a)
  do
    chain[context] = chain[context] or {}
    for token,c in pairs(b)
    do
      chain[context][token] = chain[context][token] or { weight = 0, attribution = {} }
      chain[context][token].weight = chain[context][token].weight + c.weight
      for attribution,weight in pairs(c.attribution)
      do
        chain[context][token].attribution[attribution] = chain[context][token].attribution[attribution] or 0
        chain[context][token].attribution[attribution] = chain[context][token].attribution[attribution] + weight
      end
    end
  end
end

local function deleteFrom(chain, bleep) -- mutates chain
  for context,a in pairs(chain) -- deleting or modifying existing keys is safe
  do
    for ctx,b in pairs(bleep)
    do
      if string.find(context, ctx) ~= nil
      then
        do
          chain[context] = nil
          break
        end
      end
    end
  end
end

local function empty(t)
  for _ in pairs(t)
  do
    return false
  end
  return true
end

local function prune(chain) -- mutates chain
  local changed = true
  while changed
  do
    changed = false
    for context,a in pairs(chain) -- deleting or modifying existing keys is safe
    do
      for token,b in pairs(a)
      do
        local next = string.sub(context .. token, 2, 2 + string.len(context))
        if chain[next] == nil
        then
          do
            chain[context][token] = nil
            changed = true
          end
        end
      end
      if empty(chain[context])
      then
        do
          chain[context] = nil
          changed = true
        end
      end
    end
  end
end

local function export(file, chain)
  file:write(pretty(chain))
end

local function import(file)
  return parse(file:read("*all"))
end

local function generate(chain, prompt)
  io.write(
    "<!DOCTYPE html>\n<html>\n <head>\n" ..
    "  <meta charset='UTF-8' />\n" ..
    "  <title>" .. htmlSafe(prompt) .. "</title>\n" ..
    "  <meta name='generator' content='attributive-markov-chain.lua' />\n" ..
    "  <script src='attributive-markov-chain.js'></script>\n" ..
    "  <style>\n" ..
    "   span { display: inline; font-family: monospace, monospace; }\n" ..
    "   #attribution { position: fixed; top: 0; right: 0; width: 20%; display: block; }\n" ..
    "   dl { display: none; }\n" ..
    "   #attribution dl { display: block; }\n" ..
    "  </style>\n" ..
    "  <h1>" .. htmlSafe(prompt) .. "</h1>\n\n" ..
    "  <div id='attribution'>\n" ..
    "    select text to see attribution (requires JavaScript)\n" ..
    "  </div>\n\n  "
  )
  io.write("<span>" .. prompt .. "</span>")
  io.write("<dl><dt>100.000%</dt><dd>(prompt)</dd></dl>")
  local context = prompt
  for count = 1, 10000, 1
  do
    if chain[context] and not empty(chain[context])
    then
      do
        local total = 0
        for token,m in pairs(chain[context])
        do
          total = total + m.weight
        end
        local limit = math.random() * total
        total = 0
        local picked = nil
        for token,m in pairs(chain[context])
        do
          if total < limit then
            picked = token
          end
          total = total + m.weight
        end
        if picked
        then
          do
        io.write("<span>" .. htmlSafe(picked) .. "</span>")
        total = 0
        for attr,weight in pairs(chain[context][picked].attribution)
        do
          total = total + weight
        end
        local scale = 100.0 / total
        local attribution = { }
        for attr,weight in pairs(chain[context][picked].attribution)
        do
          table.insert(attribution, { weight = weight * scale, attr = attr })
        end
        table.sort(attribution, function(a,b) return a.weight > b.weight end)
        io.write("<dl>")
        for _,a in ipairs(attribution)
        do
          io.write("<dt>" .. string.format("%.3f", a.weight) .. "%</dt>")
          io.write("<dd>" .. a.attr .. "</dd>")
        end
        io.write("</dl>")
        context = string.sub(context .. picked, 2, 2 + string.len(context))
          end
        else
          do
        io.write("<span>" .. prompt .. "</span>")
        io.write("<dl><dt>100.000%</dt><dd>(prompt)</dd></dl>")
        context = prompt
          end
        end
      end
    else
      do
        io.write("<span>" .. prompt .. "</span>")
        io.write("<dl><dt>100.000%</dt><dd>(prompt)</dd></dl>")
        context = prompt
      end
    end
  end
  io.write("\n\n </body>\n</html>\n")
end

-- main

if #arg > 1 and arg[1] == "help"
then
  do
    io.stderr:write("usage:\n" .. usage)
    return 0
  end
elseif #arg > 3 and arg[1] == "build"
then
  do
    local order = tonumber(arg[3])
    local chain = { }
    for index,path in ipairs(arg)
    do
      if (index > 3)
      then
        do
          mergeInto(chain, compile(order, readText(path)))
        end
      end
    end
    table.save(chain, arg[2])
    return 0
  end
elseif #arg > 2 and arg[1] == "generate"
then
  do
    local chain = table.load(arg[2])
    local prompt = arg[3]
    generate(chain, prompt)
    return 0
  end
elseif #arg > 3 and arg[1] == "censor"
then
  do
    local chain = table.load(arg[2])
    local order = tonumber(arg[3])
    for index,phrase in ipairs(arg)
    do
      if (index > 3)
      then
        do
          deleteFrom(chain, compile(order, { content = phrase, attribution = "(gone)" }))
        end
      end
    end
    table.save(chain, arg[2])
    return 0
  end
elseif #arg >= 2 and arg[1] == "prune"
then
  do
    local chain = table.load(arg[2])
    prune(chain)
    table.save(chain, arg[2])
    return 0
  end
else
  do
    io.stderr:write("usage:\n" .. usage)
    return 1
  end
end
