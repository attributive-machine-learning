#!/bin/bash
cat << EOF
This script will download ~95GB of music from the Internet Archive:
you need unarchive_collection from <https://mathr.co.uk/unarchive>.
It will then calculate audio rhythm fingerprints:
you need rhythm-analysis from <https://code.mathr.co.uk/disco>.
You may wish to mount a RAM disk (tmpfs) and set the TMPDIR variable.
Then it will randomly sample 110 tracks each from the top 8 genres:
final output will be in __genres__/__combined__.csv
EOF
tmp="$(mktemp -d)"
mkdir -p netlabels __meta__ __genres__
cd netlabels
unarchive_collection 20twelve 4weed aaahh aklass archipel auflegware-label bumpfoot diggarama_label digital-diamonds dubophonic ear earstroke edensonic exegene fragment gosub10 heavy_industries inoquo jahtari norbumusic phasetech planet-terror-records rebound restlabel stadtgruen zardonic
for collection in "${@}"
do
  for item in "${collection}"/*/
  do
    mkdir -p "../__meta__/${item}"
    for track in "${item}"/*.mp3 "${item}"/*.ogg "${item}"/*.flac "${item}"/*.wav "${item}"/*.aiff
    do
      if [ -e "${track}" ]
      then
        printf "%s\n" "${track}"
        [ -e "../__meta__/${track}.rhythm-analysis" ] ||
        {
          ffmpeg -i "${track}" -af loudnorm -codec:a pcm_f32le -map_metadata -1 -y "${tmp}/audio.caf" 2>/dev/null >/dev/null </dev/null
          rhythm-analysis "${tmp}/audio.caf" "../__meta__/${track}.rhythm-analysis" 2>/dev/null >/dev/null </dev/null
        }
        [ -e "../__meta__/${track}.tags.json" ] ||
        {
          ffprobe -of json -show_entries format_tags -i "${track}" > "../__meta__/${track}.tags.json" 2>/dev/null </dev/null
        }
      fi
    done
  done
done
rm "${tmp}/audio.caf"
cd "../__meta__/"
rm "../__genres__/__train__.csv" "../__genres__/__test__.csv"
for genre in "Techno" "Dub" "Indie" "House" "Ambient" "Drum & Bass" "Reggae" "Trip-Hop"
do
  grep -l -F '"genre": "'"${genre}"'"' */*/*.tags.json |
  sed "s|.tags.json$||g" |
  shuf |
  head -n 110 |
  tee >(head -n 100 > "../__genres__/${genre}.train.list") |
  tail -n 10 > "../__genres__/${genre}.test.list"
  for list in "../__genres__/${genre}.train.list" "../__genres__/${genre}.test.list"
  do
    while read file
    do
      netlabel="$(echo "${file}" | sed 's|/.*$||g')"
      tr "\n" "," < "${file}.rhythm-analysis" |
      tr -s "," |
      cut -d, -f 1-121 |
      {
        read data
        echo "${data},${genre},${netlabel},${file}"
      }
    done < "${list}" > "${list%list}csv"
  done
done
cat "../__genres__/"*".train.csv" > "../__genres__/__train__.csv"
cat "../__genres__/"*".test.csv" > "../__genres__/__test__.csv"
cat "../__genres__/__train__.csv" "../__genres__/__test__.csv" > "../__genres__/__combined__.csv"
cd ..
