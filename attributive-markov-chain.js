/*
attributive-markov-chain.js -- Markov chain that attributes its sources
Copyright (c) 2022 Claude Heiland-Allen
SPDX-License-Identifier: AGPL-3.0-only
*/

document.onselectionchange = () => {
  const sel = document.getSelection();
  const rng = sel.getRangeAt(0);
  var start = rng.startContainer;
  if (start.nodeName == '#text')
  {
    start = start.parentNode;
  }
  if (start.nodeName == 'DT' || start.nodeName == 'DD')
  {
    start = start.parentNode;
  }
  if (start.nodeName == 'DL')
  {
    start = start.nextSibling;
  }
  var end = rng.endContainer;
  if (end.nodeName == '#text')
  {
    end = end.parentNode;
  }
  if (end.nodeName == 'DT' || end.nodeName == 'DD')
  {
    end = end.parentNode;
  }
  if (end.nodeName == 'DL')
  {
    end = end.nextSibling;
  }
  if (start.nodeName == 'SPAN' && end.nodeName == 'SPAN')
  {
    var attribution = { };
    for (var node = start; node != end; node = node.nextSibling)
    {
      if (node.nodeName == 'DL')
      {
        var weight = 0;
        for (var child = node.firstChild; child; child = child.nextSibling)
        {
          if (child.nodeName == 'DT')
          {
            weight = parseFloat(child.innerText);
          }
          else if (child.nodeName == 'DD')
          {
            const key = child.innerHTML;
            attribution[key] = (attribution[key] || 0) + weight;
            weight = 0;
          }
        }
      }
    }
    const scale = 100.0 / Object.values(attribution).reduce((a, b) => a + b, 0);
    var array = [ ];
    var i = 0;
    for (const key of Object.keys(attribution))
    {
      array[i++] = [ attribution[key] * scale, key ];
    }
    array.sort((a, b) => b[0] - a[0]);
    var html = '<dl>';
    for (const [weight, key] of array)
    {
      html += '<dt>' + weight.toFixed(3) + '%</dt>';
      html += '<dd>' + key + '</dd>';
    }
    html += '</dl>';
    document.getElementById('attribution').innerHTML = html;
  }
};
