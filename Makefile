all:	attributive-markov-chain attributive-markov-chain.luac index.html

clean:
	-rm attributive-markov-chain attributive-markov-chain.hi attributive-markov-chain.o attributive-markov-chain.luac index.html

attributive-markov-chain: attributive-markov-chain.hs
	ghc -O2 attributive-markov-chain.hs

attributive-markov-chain.luac: attributive-markov-chain.lua
	luac -o attributive-markov-chain.luac attributive-markov-chain.lua

index.html: README.md
	pandoc README.md --standalone -c "style.css" --toc -o "index.html"
