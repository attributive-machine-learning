#!/bin/sh
tmux join-pane -t :0 >/dev/null 2>/dev/null
tmux select-layout -t :0 even-vertical >/dev/null 2>/dev/null
sleep 0.1
exec "$@"
