{-
attributive-markov-chain.hs -- Markov chain that attributes its sources
Copyright (c) 2022 Claude Heiland-Allen
SPDX-License-Identifier: AGPL-3.0-only
-}

import Control.Monad (forM)
import Data.List (sort, tails)
import Numeric (showFFloat)
import System.Environment (getArgs)
import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)
import System.Random (StdGen, newStdGen, random)
import qualified Data.Map as M

usage :: String
usage = "runghc attributive-markov-chain.hs prompt source ... > out.html"

type Weight = Rational
type Attribution = M.Map FilePath Weight
type Text = (String, Attribution)
type MarkovChain = M.Map [String] (M.Map String (Weight, Attribution))

content :: Text -> String
content = fst

attribution :: Text -> Attribution
attribution = snd

swap :: (a, b) -> (b, a)
swap (a, b) = (b, a)

readText :: FilePath -> IO Text
readText file = do
  c <- readFile file
  return (c, M.singleton (htmlSafe file) 1)

tokens :: Text -> [Text] -- future version may do word splitting?
tokens (c, a) = map (\w -> (w, a)) $ map (:[]) c

compile :: Int -> [Text] -> MarkovChain
compile order
  = merges
  . map singleton
  . filter ((order ==) . length)
  . map (take order)
  . tails

singleton :: [Text] -> MarkovChain
singleton context
  = M.singleton
      (map content (init context))
      (M.singleton (content (last context)) (1, attribution (last context)))

merges :: [MarkovChain] -> MarkovChain
merges = foldr1 merge

merge :: MarkovChain -> MarkovChain -> MarkovChain
merge = M.unionWith (M.unionWith m)
  where
    m (xw, xa) (yw, ya) = (xw + yw, M.unionWith (+) xa ya)

pick :: Ord k => M.Map k Weight -> Rational {- in [0..1) -} -> k
pick m = \t -> fst (M.elemAt (ix t) m)
  where
    totals = scanl1 (+) (M.elems m)
    total = last totals
    ix t = length (filter (< t * total) totals)

normalize :: Ord k => M.Map k Weight -> M.Map k Weight
normalize m = let total = sum (M.elems m) in fmap (/ total) m

generateText :: MarkovChain -> StdGen -> [Text] -> [Text]
generateText chain prng0 prompt0 = prompt0 ++ go prng0 prompt0
  where
    go prng prompt = case map content prompt `M.lookup` chain of
      Nothing -> prompt0 ++ go prng prompt0 -- reached a dead end, restart
      Just mswa ->
        let (t, prng') = random prng :: (Double, StdGen)
            s = pick (M.map fst mswa) (toRational t)
            next = (s, snd (mswa M.! s))
            prompt' = tail (prompt ++ [next])
        in  next : go prng' prompt'

-- are these really 100% safe?

htmlSafe :: String -> String
htmlSafe = concatMap htmlSafe1

htmlSafe1 :: Char -> String
htmlSafe1 ' ' = "&nbsp;"
htmlSafe1 '\n' = "<br />"
htmlSafe1 '&' = "&amp;"
htmlSafe1 '<' = "&lt;"
htmlSafe1 '>' = "&gt;"
htmlSafe1 '"' = "&quot;"
htmlSafe1 '\'' = "&apos;"
htmlSafe1 c = [c]

-- simple HTML output via string concatenation
-- overall attribution shown at start
-- hovering tokens shows specific attribution

outputHTML :: String -> [Text] -> String
outputHTML title text = header ++ concatMap details text ++ footer
  where
    header
      = "<!DOCTYPE html>\n<html>\n <head>\n" ++
        "  <meta charset='UTF-8' />\n" ++
        "  <title>" ++ htmlSafe title ++ "</title>\n" ++
        "  <meta name='generator' content='attributive-markov-chain.hs' />\n" ++
        "  <script src='attributive-markov-chain.js'></script>\n" ++
        "  <style>\n" ++
        "   span { display: inline; font-family: monospace, monospace; }\n" ++
        "   #attribution { position: fixed; top: 0; right: 0; width: 20%; display: block; }\n" ++
        "   dl { display: none; }\n" ++
        "   #attribution dl { display: block; }\n" ++
        "  </style>\n" ++
        "  <h1>" ++ htmlSafe title ++ "</h1>\n\n" ++
        "  <div id='attribution'>\n" ++
        "    select text to see attribution (requires JavaScript)\n" ++
        "  </div>\n\n  "
    footer = "\n\n </body>\n</html>\n"
    details (str, attr)
      = "<span>" ++ htmlSafe str ++ "</span>" ++
        "<dl>" ++ attribute attr ++ "</dl>"
    attribute m =
      let wfs = reverse . sort . map swap . M.assocs $ m
          weights = map fst wfs
          total = sum weights
          percentages = map (\w -> realToFrac $ 100 * w / total) weights
          sources = map snd wfs
          dtdd percent source
            = "<dt>" ++ showFFloat (Just 3) percent "" ++ "%</dt>" ++
              "<dd>" ++ source ++ "</dd>"
      in  concat $ zipWith dtdd percentages sources

main :: IO ()
main = do
  prng <- newStdGen
  args <- getArgs
  case args of
    [] -> hPutStrLn stderr ("usage: " ++ usage) >> exitFailure
    (sprompt:filenames) -> do
      let prompt = tokens (sprompt, M.singleton "(prompt)" 1)
          order = length prompt
          tokenCount = 10000
      chain <- fmap merges . forM filenames $ \file -> do
        (compile (order + 1) . tokens) `fmap` readText file
      putStr . outputHTML sprompt . map (fmap normalize) .
        take tokenCount . generateText chain prng $ prompt
